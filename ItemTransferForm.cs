﻿using System;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using System.Data;
using System.Data.SqlClient;
using SOM5.Plugins.MainApplicationGUI;
using DevExpress.XtraEditors;
using System.Net.Http;
using System.Collections.Generic;
using SOM5.Classes.EMail;
using CodeForce.Utilities;

namespace som4Autosend
{
    public partial class ItemTransferForm : Form
    {
        string Conn = "Server=10.0.1.3;Database=SOM5TestEnv;User Id=sa;Password = Lov99tea;";
        string UserName, OppType, FirstName, LastName, Email, Company, Zip, Phone, Parent;
        private static readonly HttpClient Client = new HttpClient();
        int ErrorTries = 0; //Number of times the API failed in a row

        public ItemTransferForm()
        {
            InitializeComponent();
        }

        private void emailListBtn_Click(object sender, EventArgs e)
        {
            emailListForm form = new emailListForm(Conn);
            form.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// This function serves as a utility function when needing to hash a password. Most common use when
        /// changing the Administor password to update the registry
        /// </summary>
        private void encryptStringBtn_Click(object sender, EventArgs e)
        {
            encryptForm form = new encryptForm();
            form.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM Item_Transfer WHERE Transfered = 0";
            DataTable itemsToTransfer = new DataTable();
            itemsToTransfer = getData(sql, itemsToTransfer);
            transferGridControl.DataSource = itemsToTransfer;

            foreach (DataRow row in itemsToTransfer.Rows)
            {
                bool accountExists = checkAccount(Convert.ToInt32(row["IBload"]));
                if (accountExists)
                {
                    //Account exists. Do account stuff (TBD)
                    transferItems();
                }
                else
                {
                    //Somehow no account exists since we got here - Create one
                    createAccount(Convert.ToInt32(Convert.ToInt32(row["AccountID"])));
                    transferItems();
                }
            }

            //checkForNewAccounts(); // Will check and create new accounts in Makor

        }

        /// <summary>
        /// Locally check if the account exists in Makor by passing the load id, and checking if Account_ShareID is not null
        /// </summary>
        /// <param name="loadID">int load id</param>
        private bool checkAccount(int loadID)
        {
            string sql = @"SELECT L.OID,L.load_Account, A.Account_ShareID, A.account_and_parents, l.Load_ShareID
                          FROM [Loads] L
                          Inner Join Accounts A on l.load_Account = A.OID
                          Where L.OID = " + loadID;
            try
            {
                using (SqlConnection connection = new SqlConnection(Conn))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        bool exists = (bool)cmd.ExecuteScalar();
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Creates the account in Makor if there is no Account_ShareID in Ops (no Makor account exists)
        /// </summary>
        /// <param name="accountID">int accountID</param>
        private async void createAccount(int accountID)
        {
            //First, get the account information from Ops
            DataTable accountInfo = new DataTable();
            string sql = @"SELECT 
                  A.OID, A.Account_ShareID, A.account_Parent, A.account_and_parents as Account
                  ,C.OID, C.contactFirstName as FirstName, C.contactLastName as LastName, C.contactEMailAddress as Email, C.contactPhone as Phone
                  ,Addy.Address_Line1, Addy.Address_Line2, Addy.Address_Line3, Addy.City, Addy.County, Addy.[State], Addy.Postal as Zip, A.account_Name as Account
	                FROM Accounts A
	                    Inner Join [Address] Addy on Addy.OID = A.[Primary]
		                Inner Join AccountContact AcC on AcC.Accounts = A.OID
		                Inner Join Contacts C on C.OID = AcC.Contacts
	                Where A.OID = " + accountID + " and AcC.relationPrimaryContact = 1";
            accountInfo = getData(sql, accountInfo);
            try
            {
                foreach (DataRow row in accountInfo.Rows)
                {
                    Company = row["Account"].ToString();
                    FirstName = row["FirstName"].ToString();
                    LastName = row["LastName"].ToString();
                    Email = row["Email"].ToString();
                    Phone = row["Phone"].ToString();
                    Zip = row["Zip"].ToString();

                    var values = new Dictionary<string, string>
                {
                    {"UserName", Company},
                    {"OppType", "0"},
                    {"FirstName", FirstName},
                    {"LastName", LastName},
                    {"Email", Email},
                    {"Company", Company},
                    {"Zip", Zip},
                    {"Phone", Phone},
                    {"Parent", "(need this)"}
                };

                    var content = new FormUrlEncodedContent(values);

                    var response = await Client.PostAsync("https://merpapi.makorerp.com/CreateAccount", content);

                    var responseString = await response.Content.ReadAsStringAsync();
                }
            }
            catch(Exception ex)
            {
                sendEmail(ex, "Account Creation");
            }
            
        }

        /// <summary>
        /// Checks if an account is created in Ops but not in Makor. If so, creates the account in Malpr/
        /// </summary>
        private void checkForNewAccounts()
        {
            DataTable accountsToTransfer = new DataTable();
            string sql = @"SELECT OID FROM Accounts WHERE ClientType = 'Business' AND Account_ShareID IS NULL AND activeAccount = 1";
            accountsToTransfer = getData(sql, accountsToTransfer);
            foreach (DataRow row in accountsToTransfer.Rows)
            {
                createAccount(Convert.ToInt32(row["OID"]));
            }
        }

        /// <summary>
        /// Transfers items from Ops to Makor
        /// </summary>
        private void transferItems()
        {
            try
            {
                //Try to transfer the item

                ErrorTries = 0; //If transfer was successful, reset error counter
            }catch (Exception ex)
            {
                ErrorTries++;
                if(ErrorTries % 6 == 0){
                    //Send email for every 6th failure
                    sendEmail(ex, "Item Transfer");
                }
            }
        }

        private DataTable getData(string sql, DataTable table)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(Conn))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(sql, con))
                    {
                        adapter.Fill(table);
                    }
                }
            }catch (Exception ex){
                sendEmail(ex);
            }
            return table;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            timer1_Tick(null, null);
        }

        /// <summary>
        /// Sends an email on any transfer of data error
        /// </summary>
        /// <param name="ex">Excecption ex of the issue</param>
        /// <param name="process">string process of the attempt type being made. Is nullable.</param>
        private void sendEmail(Exception ex, string process = null)
        {
            //Send Email Notification
            string emailQuery = @"SELECT email
                                      FROM [AlertGroup] AG
                                      inner join AlertUsers AU on AU.groups = AG.OID
                                      inner join Users U on AU.users = U.OID
                                      where groupName = 'MakorIntegration' ";
            emaillibrary emailer = new emaillibrary();
            DataTable emailTable = new DataTable();
            List<string> emailList = new List<string>();

            using (SqlConnection con = new SqlConnection(MainApplicationGUIPlugin.connectionString))
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter(emailQuery, con))
                {
                    adapter.Fill(emailTable);
                }
            }

            foreach (DataRow row in emailTable.Rows)
            {
                if (row["email"].ToString() != "")
                {
                    emailList.Add(row["email"].ToString());
                }
            }

            emailer.SendEMail("Makor Transfer Error -" + process, @"The following error was captured 
                from the Makor Integration application: '"  + ex.Message + "'" , emailList, null, null, null);
        }
    }    
}
