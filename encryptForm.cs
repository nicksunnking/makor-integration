﻿using System;
using System.Windows.Forms;
using CodeForce.Utilities;

namespace som4Autosend
{
    public partial class encryptForm : DevExpress.XtraEditors.XtraForm
    {
        public encryptForm()
        {
            InitializeComponent();
            saltTextBox.Text = "2MVtfR3F?-(K,gv8x`>S81&Y-GCw:$(*u,=7jMw3%VwUM[_2M4et9CF0ZV&tNQ,<"; //default salt key

            ToolTip clipboardTooltip = new ToolTip();
            clipboardTooltip.ShowAlways = true;
            clipboardTooltip.SetToolTip(copyToClipBoardBtn, "Copy To Clipboard");

            ToolTip refreshTooltip = new ToolTip();
            refreshTooltip.ShowAlways = true;
            refreshTooltip.SetToolTip(refreshBtn, "Refresh Salt");
        }

        /// <summary>
        /// Encrypt a string
        /// </summary>
        private void encryptBtn_Click(object sender, EventArgs e)
        {
            resultTextBox.Text = EncyptionUtility.Encrypt(stringTextBox.Text, saltTextBox.Text);
        }

        /// <summary>
        /// Decrypt a string
        /// </summary>
        private void decryptBtn_Click(object sender, EventArgs e)
        {
            resultTextBox.Text = EncyptionUtility.Decrypt(stringTextBox.Text, saltTextBox.Text);
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            stringTextBox.Text = "";
            saltTextBox.Text = "2MVtfR3F?-(K,gv8x`>S81&Y-GCw:$(*u,=7jMw3%VwUM[_2M4et9CF0ZV&tNQ,<"; //default salt key
            resultTextBox.Text = "";
        }

        private void copyToClipBoardBtn_Click(object sender, EventArgs e)
        {
            if (resultTextBox.Text != "")
            {
                Clipboard.SetText(resultTextBox.Text);
            }
            
        }
    }
}