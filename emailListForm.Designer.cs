﻿namespace som4Autosend
{
    partial class emailListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.excludedUserListBox = new System.Windows.Forms.ListBox();
            this.includedUserListBox = new System.Windows.Forms.ListBox();
            this.includedLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.removeBtn = new System.Windows.Forms.Button();
            this.addBtn = new System.Windows.Forms.Button();
            this.clearIncludedSelectedBtn = new System.Windows.Forms.Button();
            this.clearExcludedSelectedBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // excludedUserListBox
            // 
            this.excludedUserListBox.FormattingEnabled = true;
            this.excludedUserListBox.Location = new System.Drawing.Point(12, 41);
            this.excludedUserListBox.Name = "excludedUserListBox";
            this.excludedUserListBox.Size = new System.Drawing.Size(217, 368);
            this.excludedUserListBox.TabIndex = 0;
            // 
            // includedUserListBox
            // 
            this.includedUserListBox.FormattingEnabled = true;
            this.includedUserListBox.Location = new System.Drawing.Point(387, 41);
            this.includedUserListBox.Name = "includedUserListBox";
            this.includedUserListBox.Size = new System.Drawing.Size(217, 368);
            this.includedUserListBox.TabIndex = 1;
            // 
            // includedLabel
            // 
            this.includedLabel.AutoSize = true;
            this.includedLabel.Font = new System.Drawing.Font("Tahoma", 12F);
            this.includedLabel.Location = new System.Drawing.Point(41, 4);
            this.includedLabel.Name = "includedLabel";
            this.includedLabel.Size = new System.Drawing.Size(116, 19);
            this.includedLabel.TabIndex = 2;
            this.includedLabel.Text = "Excluded Users";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.includedLabel);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(217, 31);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(387, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(217, 31);
            this.panel2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.label1.Location = new System.Drawing.Point(54, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Added Users";
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(332, 185);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(49, 42);
            this.removeBtn.TabIndex = 5;
            this.removeBtn.Text = "<-";
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(235, 185);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(49, 42);
            this.addBtn.TabIndex = 6;
            this.addBtn.Text = "->";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // clearIncludedSelectedBtn
            // 
            this.clearIncludedSelectedBtn.Location = new System.Drawing.Point(447, 419);
            this.clearIncludedSelectedBtn.Name = "clearIncludedSelectedBtn";
            this.clearIncludedSelectedBtn.Size = new System.Drawing.Size(85, 23);
            this.clearIncludedSelectedBtn.TabIndex = 7;
            this.clearIncludedSelectedBtn.Text = "Clear Selected";
            this.clearIncludedSelectedBtn.UseVisualStyleBackColor = true;
            this.clearIncludedSelectedBtn.Click += new System.EventHandler(this.clearIncludedSelectedBtn_Click);
            // 
            // clearExcludedSelectedBtn
            // 
            this.clearExcludedSelectedBtn.Location = new System.Drawing.Point(70, 419);
            this.clearExcludedSelectedBtn.Name = "clearExcludedSelectedBtn";
            this.clearExcludedSelectedBtn.Size = new System.Drawing.Size(91, 23);
            this.clearExcludedSelectedBtn.TabIndex = 8;
            this.clearExcludedSelectedBtn.Text = "Clear Selected";
            this.clearExcludedSelectedBtn.UseVisualStyleBackColor = true;
            this.clearExcludedSelectedBtn.Click += new System.EventHandler(this.clearExcludedSelectedBtn_Click);
            // 
            // emailListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 454);
            this.Controls.Add(this.clearExcludedSelectedBtn);
            this.Controls.Add(this.clearIncludedSelectedBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.includedUserListBox);
            this.Controls.Add(this.excludedUserListBox);
            this.Name = "emailListForm";
            this.Text = "emailListForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox excludedUserListBox;
        private System.Windows.Forms.ListBox includedUserListBox;
        private System.Windows.Forms.Label includedLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button clearIncludedSelectedBtn;
        private System.Windows.Forms.Button clearExcludedSelectedBtn;
    }
}