﻿namespace som4Autosend
{
    partial class encryptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(encryptForm));
            this.encryptLabel = new System.Windows.Forms.Label();
            this.stringTextBox = new System.Windows.Forms.TextBox();
            this.saltTextBox = new System.Windows.Forms.TextBox();
            this.saltLabel = new System.Windows.Forms.Label();
            this.stringLabel = new System.Windows.Forms.Label();
            this.resultLabel = new System.Windows.Forms.Label();
            this.encryptBtn = new System.Windows.Forms.Button();
            this.decryptBtn = new System.Windows.Forms.Button();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.copyToClipBoardBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // encryptLabel
            // 
            this.encryptLabel.AutoSize = true;
            this.encryptLabel.Font = new System.Drawing.Font("Tahoma", 16F);
            this.encryptLabel.Location = new System.Drawing.Point(346, 56);
            this.encryptLabel.Name = "encryptLabel";
            this.encryptLabel.Size = new System.Drawing.Size(167, 27);
            this.encryptLabel.TabIndex = 0;
            this.encryptLabel.Text = "Encrypt A String";
            // 
            // stringTextBox
            // 
            this.stringTextBox.Font = new System.Drawing.Font("Tahoma", 16F);
            this.stringTextBox.Location = new System.Drawing.Point(151, 128);
            this.stringTextBox.Name = "stringTextBox";
            this.stringTextBox.Size = new System.Drawing.Size(634, 33);
            this.stringTextBox.TabIndex = 1;
            // 
            // saltTextBox
            // 
            this.saltTextBox.Font = new System.Drawing.Font("Tahoma", 16F);
            this.saltTextBox.Location = new System.Drawing.Point(151, 188);
            this.saltTextBox.Name = "saltTextBox";
            this.saltTextBox.Size = new System.Drawing.Size(634, 33);
            this.saltTextBox.TabIndex = 2;
            // 
            // saltLabel
            // 
            this.saltLabel.AutoSize = true;
            this.saltLabel.Font = new System.Drawing.Font("Tahoma", 16F);
            this.saltLabel.Location = new System.Drawing.Point(51, 188);
            this.saltLabel.Name = "saltLabel";
            this.saltLabel.Size = new System.Drawing.Size(48, 27);
            this.saltLabel.TabIndex = 3;
            this.saltLabel.Text = "Salt";
            // 
            // stringLabel
            // 
            this.stringLabel.AutoSize = true;
            this.stringLabel.Font = new System.Drawing.Font("Tahoma", 16F);
            this.stringLabel.Location = new System.Drawing.Point(51, 131);
            this.stringLabel.Name = "stringLabel";
            this.stringLabel.Size = new System.Drawing.Size(68, 27);
            this.stringLabel.TabIndex = 4;
            this.stringLabel.Text = "String";
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Tahoma", 16F);
            this.resultLabel.Location = new System.Drawing.Point(51, 289);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(72, 27);
            this.resultLabel.TabIndex = 5;
            this.resultLabel.Text = "Result";
            // 
            // encryptBtn
            // 
            this.encryptBtn.Font = new System.Drawing.Font("Tahoma", 14F);
            this.encryptBtn.Location = new System.Drawing.Point(334, 346);
            this.encryptBtn.Name = "encryptBtn";
            this.encryptBtn.Size = new System.Drawing.Size(135, 56);
            this.encryptBtn.TabIndex = 6;
            this.encryptBtn.Text = "Encrypt";
            this.encryptBtn.UseVisualStyleBackColor = true;
            this.encryptBtn.Click += new System.EventHandler(this.encryptBtn_Click);
            // 
            // decryptBtn
            // 
            this.decryptBtn.Font = new System.Drawing.Font("Tahoma", 14F);
            this.decryptBtn.Location = new System.Drawing.Point(475, 346);
            this.decryptBtn.Name = "decryptBtn";
            this.decryptBtn.Size = new System.Drawing.Size(137, 56);
            this.decryptBtn.TabIndex = 7;
            this.decryptBtn.Text = "Decrypt";
            this.decryptBtn.UseVisualStyleBackColor = true;
            this.decryptBtn.Click += new System.EventHandler(this.decryptBtn_Click);
            // 
            // resultTextBox
            // 
            this.resultTextBox.Font = new System.Drawing.Font("Tahoma", 16F);
            this.resultTextBox.Location = new System.Drawing.Point(142, 286);
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ReadOnly = true;
            this.resultTextBox.Size = new System.Drawing.Size(643, 33);
            this.resultTextBox.TabIndex = 8;
            // 
            // refreshBtn
            // 
            this.refreshBtn.Image = ((System.Drawing.Image)(resources.GetObject("refreshBtn.Image")));
            this.refreshBtn.Location = new System.Drawing.Point(618, 346);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(74, 56);
            this.refreshBtn.TabIndex = 9;
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // copyToClipBoardBtn
            // 
            this.copyToClipBoardBtn.Image = ((System.Drawing.Image)(resources.GetObject("copyToClipBoardBtn.Image")));
            this.copyToClipBoardBtn.Location = new System.Drawing.Point(698, 346);
            this.copyToClipBoardBtn.Name = "copyToClipBoardBtn";
            this.copyToClipBoardBtn.Size = new System.Drawing.Size(87, 56);
            this.copyToClipBoardBtn.TabIndex = 10;
            this.copyToClipBoardBtn.UseVisualStyleBackColor = true;
            this.copyToClipBoardBtn.Click += new System.EventHandler(this.copyToClipBoardBtn_Click);
            // 
            // encryptForm
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 461);
            this.Controls.Add(this.copyToClipBoardBtn);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.resultTextBox);
            this.Controls.Add(this.decryptBtn);
            this.Controls.Add(this.encryptBtn);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.stringLabel);
            this.Controls.Add(this.saltLabel);
            this.Controls.Add(this.saltTextBox);
            this.Controls.Add(this.stringTextBox);
            this.Controls.Add(this.encryptLabel);
            this.Name = "encryptForm";
            this.Text = "encryptForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label encryptLabel;
        private System.Windows.Forms.TextBox stringTextBox;
        private System.Windows.Forms.TextBox saltTextBox;
        private System.Windows.Forms.Label saltLabel;
        private System.Windows.Forms.Label stringLabel;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button encryptBtn;
        private System.Windows.Forms.Button decryptBtn;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.Button copyToClipBoardBtn;
    }
}