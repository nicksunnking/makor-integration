﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace som4Autosend
{
    public partial class emailListForm : DevExpress.XtraEditors.XtraForm
    {
        string Conn; //connection string
        DataTable excludedTable;
        string excludedSQL = @"SELECT CONCAT(OID, ' - ', displayName) AS [User] FROM users WHERE OID NOT IN(
                            SELECT U.OID
                            FROM [AlertGroup] AG
                            INNER JOIN AlertUsers AU on AU.groups = AG.OID
                            INNER JOIN Users U on AU.users = U.OID
                            where groupName = 'MakorIntegration'
                            )ORDER BY displayName ASC";

        DataTable includedTable;
        string includedSQL = @"SELECT CONCAT(u.oid, ' - ', displayName) AS [User]
                        FROM [AlertGroup] AG
                        INNER JOIN AlertUsers AU on AU.groups = AG.OID
                        INNER JOIN Users U on AU.users = U.OID
                        WHERE groupName = 'MakorIntegration' ORDER BY displayName ASC";

        public emailListForm(string Conn)
        {
            this.Conn = Conn;
            InitializeComponent();
            excludedUserListBox.SelectionMode = SelectionMode.MultiSimple;
            includedUserListBox.SelectionMode = SelectionMode.MultiSimple;
            getExcludedUsers(); 
            getIncludedUsers();

            //Clear selected options 
            includedUserListBox.ClearSelected();
            excludedUserListBox.ClearSelected();
        }

        /// <summary>
        /// Gets users not included in the email chain 
        /// </summary>
        private void getExcludedUsers()
        {
            excludedTable = new DataTable();
            excludedUserListBox.DataSource = getData(excludedSQL, excludedTable);
            excludedUserListBox.DisplayMember = "User";
        }

        /// <summary>
        /// Gets users  included in the email chain 
        /// </summary>
        private void getIncludedUsers()
        {
            includedTable = new DataTable();
            includedUserListBox.DataSource = getData(includedSQL, includedTable);
            includedUserListBox.DisplayMember = "User";
        }

        private DataTable getData(string sql, DataTable table)
        {

            try
            {
                using (SqlConnection con = new SqlConnection(Conn))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(sql, con))
                    {
                        adapter.Fill(table);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("No users found: " + ex.Message);
            }
            return table;
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            foreach (var item in includedUserListBox.SelectedItems)
            {
                string user = includedUserListBox.GetItemText(item);
                int userID = Convert.ToInt32(user.Split(' ')[0]);

                removeUser(userID);
            }

            getExcludedUsers();
            getIncludedUsers();

            //Clear selected options 
            includedUserListBox.ClearSelected();
            excludedUserListBox.ClearSelected();

        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            foreach (var item in excludedUserListBox.SelectedItems)
            {
                string user = excludedUserListBox.GetItemText(item);
                int userID = Convert.ToInt32(user.Split(' ')[0]);

                addUser(userID);  
            }
            getExcludedUsers();
            getIncludedUsers();

            //Clear selected options 
            includedUserListBox.ClearSelected();
            excludedUserListBox.ClearSelected();
        }

        /// <summary>
        /// Removes a user from the mailing list
        /// </summary>
        /// <param name="userID">int OID of the user</param>
        private void removeUser(int userID)
        {
            try
            {
                //Saving Load Memo
                using (SqlConnection connection = new SqlConnection(Conn))
                {
                    connection.Open();
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "DELETE FROM AlertUsers WHERE groups = 7 AND users = " + userID;
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to remove user: " + ex);
            }
        }

        /// <summary>
        /// Adds a user to the mailing list
        /// </summary>
        /// <param name="userID">int OID of the user</param>
        private void addUser(int userID)
        {
            try
            {
                //Saving Load Memo
                using (SqlConnection connection = new SqlConnection(Conn))
                {
                    connection.Open();
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO AlertUsers ([groups], [users]) VALUES (7, " + userID + ")";
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to add user: " + ex);
            }
        }

        private void clearIncludedSelectedBtn_Click(object sender, EventArgs e)
        {
            includedUserListBox.ClearSelected(); 
        }

        private void clearExcludedSelectedBtn_Click(object sender, EventArgs e)
        {
            excludedUserListBox.ClearSelected();
        }

    }
}