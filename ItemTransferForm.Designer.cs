﻿namespace som4Autosend
{
    partial class ItemTransferForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.xpCollection1 = new DevExpress.Xpo.XPCollection(this.components);
            this.session1 = new DevExpress.Xpo.Session(this.components);
            this.transferGridControl = new DevExpress.XtraGrid.GridControl();
            this.transferGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.transID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.itemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ibLoad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toPalletId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Transfered = new DevExpress.XtraGrid.Columns.GridColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.xpCollection2 = new DevExpress.Xpo.XPCollection(this.components);
            this.emailListBtn = new System.Windows.Forms.Button();
            this.encryptStringBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transferGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transferGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection2)).BeginInit();
            this.SuspendLayout();
            // 
            // xpCollection1
            // 
            this.xpCollection1.CriteriaString = "[load_type] = 1 And [CoRDsent] Is Null And [load_recievedFinish] Is Not Null And " +
    "[load_recievedFinish] >= #2016-06-21# And [CoRDError] Is Null";
            this.xpCollection1.ObjectType = typeof(SOM5.Classes.DatabaseLibrary.Loads);
            this.xpCollection1.Session = this.session1;
            // 
            // session1
            // 
            this.session1.IsObjectModifiedOnNonPersistentPropertyChange = null;
            this.session1.TrackPropertiesModifications = false;
            // 
            // transferGridControl
            // 
            this.transferGridControl.DataSource = this.xpCollection1;
            this.transferGridControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.transferGridControl.Location = new System.Drawing.Point(0, 0);
            this.transferGridControl.MainView = this.transferGridView;
            this.transferGridControl.Name = "transferGridControl";
            this.transferGridControl.Size = new System.Drawing.Size(697, 366);
            this.transferGridControl.TabIndex = 0;
            this.transferGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.transferGridView});
            // 
            // transferGridView
            // 
            this.transferGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.transID,
            this.itemID,
            this.ibLoad,
            this.toPalletId,
            this.Transfered});
            this.transferGridView.GridControl = this.transferGridControl;
            this.transferGridView.Name = "transferGridView";
            this.transferGridView.OptionsDetail.EnableMasterViewMode = false;
            // 
            // transID
            // 
            this.transID.Caption = "Trans ID";
            this.transID.FieldName = "TRANSID";
            this.transID.Name = "transID";
            this.transID.OptionsColumn.AllowEdit = false;
            this.transID.OptionsColumn.AllowFocus = false;
            this.transID.Visible = true;
            this.transID.VisibleIndex = 0;
            // 
            // itemID
            // 
            this.itemID.Caption = "Item ID";
            this.itemID.FieldName = "ItemID";
            this.itemID.Name = "itemID";
            this.itemID.OptionsColumn.AllowEdit = false;
            this.itemID.OptionsColumn.AllowFocus = false;
            this.itemID.Visible = true;
            this.itemID.VisibleIndex = 1;
            // 
            // ibLoad
            // 
            this.ibLoad.Caption = "IB Load";
            this.ibLoad.FieldName = "IBload";
            this.ibLoad.Name = "ibLoad";
            this.ibLoad.OptionsColumn.AllowEdit = false;
            this.ibLoad.OptionsColumn.AllowFocus = false;
            this.ibLoad.Visible = true;
            this.ibLoad.VisibleIndex = 2;
            // 
            // toPalletId
            // 
            this.toPalletId.Caption = "Pallet ID";
            this.toPalletId.FieldName = "ToPalletID";
            this.toPalletId.Name = "toPalletId";
            this.toPalletId.OptionsColumn.AllowEdit = false;
            this.toPalletId.OptionsColumn.AllowFocus = false;
            this.toPalletId.Visible = true;
            this.toPalletId.VisibleIndex = 3;
            // 
            // Transfered
            // 
            this.Transfered.Caption = "Finished";
            this.Transfered.FieldName = "Transfered";
            this.Transfered.Name = "Transfered";
            this.Transfered.OptionsColumn.AllowEdit = false;
            this.Transfered.OptionsColumn.AllowFocus = false;
            this.Transfered.OptionsColumn.ReadOnly = true;
            this.Transfered.Visible = true;
            this.Transfered.VisibleIndex = 4;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // xpCollection2
            // 
            this.xpCollection2.CriteriaString = "[activeAccount] = True And [accountExpire] Is Not Null";
            this.xpCollection2.ObjectType = typeof(SOM5.Classes.DatabaseLibrary.Accounts);
            this.xpCollection2.Session = this.session1;
            // 
            // emailListBtn
            // 
            this.emailListBtn.Location = new System.Drawing.Point(579, 377);
            this.emailListBtn.Name = "emailListBtn";
            this.emailListBtn.Size = new System.Drawing.Size(106, 35);
            this.emailListBtn.TabIndex = 1;
            this.emailListBtn.Text = "Email List";
            this.emailListBtn.UseVisualStyleBackColor = true;
            this.emailListBtn.Click += new System.EventHandler(this.emailListBtn_Click);
            // 
            // encryptStringBtn
            // 
            this.encryptStringBtn.Location = new System.Drawing.Point(12, 377);
            this.encryptStringBtn.Name = "encryptStringBtn";
            this.encryptStringBtn.Size = new System.Drawing.Size(103, 35);
            this.encryptStringBtn.TabIndex = 2;
            this.encryptStringBtn.Text = "Encrypt String";
            this.encryptStringBtn.UseVisualStyleBackColor = true;
            this.encryptStringBtn.Click += new System.EventHandler(this.encryptStringBtn_Click);
            // 
            // ItemTransferForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 424);
            this.Controls.Add(this.encryptStringBtn);
            this.Controls.Add(this.emailListBtn);
            this.Controls.Add(this.transferGridControl);
            this.Name = "ItemTransferForm";
            this.Text = "Item Transfers To Makor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transferGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transferGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.Xpo.XPCollection xpCollection1;
        private DevExpress.Xpo.Session session1;
        private DevExpress.XtraGrid.GridControl transferGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView transferGridView;
        private DevExpress.XtraGrid.Columns.GridColumn itemID;
        private DevExpress.XtraGrid.Columns.GridColumn transID;
        private DevExpress.XtraGrid.Columns.GridColumn ibLoad;
        private DevExpress.XtraGrid.Columns.GridColumn toPalletId;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraGrid.Columns.GridColumn Transfered;
        private DevExpress.Xpo.XPCollection xpCollection2;
        private System.Windows.Forms.Button emailListBtn;
        private System.Windows.Forms.Button encryptStringBtn;
    }
}

